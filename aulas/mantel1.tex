\section{O Teorema de Mantel para grafos aleatórios}

Seja $\ex(G,H)=\max\{e(G): G\not\supset H, v(G)=n\}$. O Teorema de Mantel pode 
ser escrito da seguinte forma.
\begin{theorem}[Mantel, 1907]
    $$\ex(G,K_3) =
    \left\lceil\frac{n}2\right\rceil\left\lfloor\frac{n}2\right\rfloor=\(\frac14+o(1)\)n^2.$$
\end{theorem}
Para o grafo aleatório $G(n,p)$ de \Erdos-Renyi, definimos 
$$\ex(G(n,p), H) = \max\{e(G), G\subset G(n,p), G\not\supset H\}.$$
Queremos provar o seguinte resultado.
\begin{theorem}[Frankl-Rodl, 1986]\label{th:mantelgnp}
    Para $p\gg 1/\sqrt{n}$, $\ex(G(n,p), K_3)=(\frac14+o(1))pn^2$ com 
    alta probabilidade.
\end{theorem}
\begin{remark}\label{rem:mantelgnp}
    Para mostrar a desigualdade $\ex(G(n,p),K_3)\geq(\frac14+o(1))pn^2$, é suficiente 
    considerar a intersecção de $G(n,p)$ com o grafo $K_{\lfloor n/2\rfloor\lceil n/2\rceil}$. 
    De fato, seja $X = e(G(n,p)\cap K_{\lfloor n/2\rfloor\lceil n/2\rceil}$.
    Temos $\EE(X) = \frac14 pn^2$ e $\Var(X) = n\frac{p}4(1-\frac{p}4)$. Logo, 
    segue da Desigualdade de Chebychev que
    $$\PP(|X-\EE(X)|\geq n)\leq \frac{\Var(X)}{n^2}=\frac{p(1-\frac{p}4)}{4n}\to 0,$$
    quando $n\to\infty$.
\end{remark}


Para demonstrar o Teorema~\ref{th:mantelgnp}, vamos precisar do resultado de 
\emph{supersaturação} abaixo, que afirma que se um grafo $G$ tem uma quantidade de arestas
significativamente maior do que $\frac14n^2$, então há muitas cópias de
triângulos em $G$.

\begin{lemma}[Supersaturação de triângulos]\label{lemma:saturacao}
    Para todo $\eps > 0$, existe $\delta>0$ tal que se um grafo $G$, $v(G)=n$, satisfaz 
    $e(G)\geq (\frac14+\eps)n^2$, então $G$ contém pelo menos $\delta \binom{n}3$ triângulos. 
\end{lemma}
\begin{proof}
    Seja $t = \lceil{\sqrt(2/\eps)}\rceil$. Pelo Teorema de Mantel, temos que 
    se $H$ é um grafo com $t$ vértices e com número de arestas
    $e(H)\geq (\frac14+\frac12\eps)t^2\geq \frac14t^2 + 1$, então $H$ contém um 
    triângulo. Vamos mostrar que podemos tomar $\delta = \frac{\eps}2{\binom{t}3}^{-1}$. 

    Seja $\cT = \binom{V(G)}{t} = \{T\subseteq V(G)\: |T|=t\}$.  Para qualquer grafo $H$, 
    definimos a \emph{densidade} $d(H)$ de $H$ como $d(H) = |e(H)|{\binom{v(H)}2}^{-1}$.
    A seguir biparticionamos $\cT$ de acordo com a densidade do grafo induzido por seus 
    elementos. Mais especificamente, definimos
    $$\cR = \left\{T\in\cT\: d(G[T])> \frac12+\frac{\epsilon}2\right\} \quad\text{ e } \quad
     \cS= \left\{T\in\cT\: d(G[T])\leq \frac12+\frac{\eps}2\right\}.$$
    Note que a densidade $d(G)$ de $G$ pode ser expressa como a média da densidade 
    dos grafos induzidos por membros de $\cT$. De fato, temos
    $$\frac{\sum_{T\in\cT}d(G[T])}{\binom{n}t} = \frac{\sum_{T\in\cT}e(G[T])}{\binom{t}2\binom{n}t}
    =\frac{e(G)\binom{n-2}{t-2}}{\binom{t}2\binom{n}t} = \frac{e(G)}{\binom{n}2} = d(G).$$
    Daí, segue que a cardinalidade $\cR$ corresponde a uma fração $\frac12\eps$
    de $\cT$. De fato, 
    $$\binom{n}{t}\(\frac12+\eps\)\leq \binom{n}{t}d(G)\leq |\cS|\cdot\(\frac12+\frac{\eps}2\) + |\cR|\cdot1$$
    o que implica $|\cR|> \frac{\eps}2\binom{n}t$. 
    
    Usamos agora o fato de $G[T]$ conter um triângulo, para todo $T\in\cR$. 
    Observamos, também, que cada um desses triângulos pertence a no máximo 
    $\binom{n-3}{t-3}$ conjuntos $T\in\cR$. Logo, o número de triângulos em $G$ é pelo menos
    $$\frac{\eps}2\binom{n}t\binom{n-3}{t-3}^{-1} =
    \frac{\eps}2{\binom{t}3}^{-1}\binom{n}3 = \delta\binom{n}3,$$
    como desejado.
\end{proof}

Para provar o Teorema~\ref{th:mantelgnp}, precisaremos também do 
Lema de Containers para triângulos, enunciado abaixo. 
\begin{lemma}[Containers para triângulos]\label{lemma:containersK3}
    Para todo $\delta>0$, existe $C>0$ tal que para todo inteiro $n$ existe 
    uma coleção $\cG$ de grafos  e uma função $f:\cP(E(K_n))\to\cG$
    satisfazendo as seguintes propriedades.
    \begin{enumerate}
        \item Para todo grafo $G$ livre de triângulos, existe $S\subseteq E(G)$, 
        $|S|\leq Cn^{3/2}$, tal que $E(G)\subseteq f(S)$.
        \item Para todo $S\subseteq E(K_n)$, o grafo $f(S)$ possui no máximo 
        $\delta n^3$ cópias de triângulos. $\hfill\qed$
    \end{enumerate}
\end{lemma}


\begin{proof}[Demonstração do Teorema~\ref{th:mantelgnp}]
    Segue da Observação~\ref{rem:mantelgnp}, que $\ex(G(n,p),K_3) \geq
    (\frac14+o(1))pn^2$. Para provar a outra desigualdade, mostraremos 
    que para todo $\eps>0$, a probabilidade de existir algum grafo $G$ livre 
    de triângulos e com pelo menos $m=(\frac14+2\eps)pn^2$ arestas contido 
    em $G(n,p)$ é arbitrariamente pequena. 

    Fixe $\eps>0$ e seja $\delta$ dado como no Lema~\ref{lemma:saturacao}.
    Seja $G$ um grafo livre de triângulos com pelo menos $m$
    arestas. Pelo Lema~\ref{lemma:containersK3}, existe $S\subseteq E(G)$, $|S|\leq Cn^{3/2}$, tal que 
    $f(S)$ possui no máximo $\delta n^3$ triângulos. Segue do Lema~\ref{lemma:saturacao}, que 
    $f(S)$ possui no máximo $(\frac14+\eps)n^2$ arestas. Agora, notamos que se um tal $G$ estivesse
    contido em $G(n,p)$, então teríamos $|f(S)\cap E(G(n,p))|\geq m$. Logo, para provarmos o resultado 
    desejado, é suficiente mostrar que $\PP(Y\geq 1)\xrightarrow{} 0$, onde 
    $$Y = \#\{S\subseteq E(G(n,p))\: |f(S)\cap E(G(n,p))|\geq m\}.$$

   Fixe $S$ e seja $X_S = f(S)\cap E(G(n,p))$. Como
   $\EE(X_S) \leq (\frac14+\eps)pn^2$, segue da desigualdade de Chernoff que 
   $$\PP(X_S \geq m-|S|) \leq \exp\{kpn^2\},$$ para 
   algum $k=k(\eps)$, uma vez que $|S|\ll \EE(X)$.
   Logo, temos 
   $$\PP(Y\geq 1)\leq \EE(Y)\leq \sum_{s=0}^{Cn^{3/2}} p^s\PP(X_S\geq m-|S|)
   \leq \sum_{s=0}^{Cn^{3/2}} p^se^{-kpn^2}
   \leq \sum_{s=0}^{Cn^{3/2}}\(\frac{epn^2}{2s}\)^se^{-kpn^2}.$$
   Usando o fato que a função $s\mapsto ({epn^2}/{2s})^s$ atinge 
   seu máximo em $s_{max}=epn^2/4\ll Cn^{3/2}$, concluímos que, 
   na soma acima, o último termo limita superiormente cada um 
   dos demais. Logo, 
   $$\PP(Y\geq 1)\leq Cn^{3/2}
   \(\frac{epn^2}{2Cn^{3/2}}\)^{Cn^{3/2}}e^{-kpn^2}\xrightarrow{}0,$$
   como desejado.
\end{proof}




    
    


