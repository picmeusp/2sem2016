\section{Aplicações do Lema de Schartz-Zippel}
Seja $X$ um conjunto finito e $\cdot:X\times X\to X$ uma operação binária
sobre $X$. Estamos interessado em verificar se $\cdot$ é uma operação 
\emph{associativa}, isto é, se para qualquer tripla $(a,b,c)\in X^3$ vale 
que $a\cdot (b\cdot c) = (a\cdot b)\cdot c$. 

Uma primeira idéia para um algoritmo para verificar se $\cdot$ é associativa 
consiste em tomar uma tripla $(a,b,c)\in X^3$ uniformemente ao acaso e 
verificar se $(a,b,c)$ é uma \emph{tripla associativa}, i.e. se 
$a\cdot(b\cdot c) = (a\cdot b)\cdot c$. Infelizmente, o exemplo 
a seguir mostra que é possível definir uma operação não-associativa $\cdot$ sobre um conjunto 
$X$ de forma que apenas uma tripla dentre as $|X|^3$ triplas é não-associativa. 

\begin{example}
    Sejam $x,y,a$ elementos distintos de um conjunto finito $X$. Defina uma operação 
    $\cdot:X\times X\to X$ fazendo $x\cdot y = x$ e $b\cdot c=a$ para todo par 
    $(b,c)\in X^2$ tal que $(b,c)\not= (x,y)$. Neste caso, temos que a tripla 
    $(x,y,y)$ é não-transitiva pois $x\cdot (y\cdot y)=a$ e $(x\cdot y)\cdot y = x$. 
    Por outro lado, é fácil verificar que para qualquer outra tripla $(\alpha,\beta,\gamma)\not=(x,y,y)$, 
    temos $\alpha\cdot(\beta\cdot\gamma)=(\alpha\cdot\beta)\cdot\gamma=a$. 
\end{example}

O exemplo acima mostra que é possível que apenas uma fração $\frac1{|X|^3}$ das triplas testemunhe
que $(X,\cdot)$ é não-associativa. Ainda assim, mostraremos que existe um algoritmo com tempo de 
execução $\textrm{O}(|X|^2)$ que é capaz de determinar com alta probabilidade se $(X,\cdot)$ é associativo. 
Para isso, usaremos o Lema de Schartz-Zippel, que é enunciado a seguir usando a linguagem de probabilidade.
\begin{lemma}[Schartz-Zippel (versão probabilística)]\label{lemma:szprob}
    Seja $p\in K[x_1,\dots,x_m]$ um polinômio de grau $\partial{p}\leq d$, 
    $S\subseteq K$ um conjunto finito e  $\vec{x}\in_U S^m$ um vetor
    escolhido uniformemente ao acaso em $S^m$. Então 
    $$\PP(p(x) = 0) \leq \frac{d}{|S|}.$$
\end{lemma}

Seja $\FF$ um corpo finito com $|\FF|\geq 6$. Considere o espaço vetorial 
$\FF^X$ de vetores com entradas em $\FF$ indexado por elementos de $X$. 
Definimos $e:X\to\FF^X$, fazendo $e(x)$ ser o vetor que possui entrada 
$1$ na coordenada indexada por $x$ e $0$ nas demais coordenadas. 

Note que fixado $u\in \FF^X$, existem $(u_x)_{x\in X}$ que satisfazem
$u = \sum_{x\in X}u_xe(x)$. Definimos uma operação binária $\boxdot:\FF^X\times \FF^X\to\FF^X$ 
da seguinte forma. Dados $u = \sum_{x\in X}u_xe(x)$ e $v\in \sum_{y\in X}v_ye(y)$, 
definimos 
$$u\boxdot v := \sum_{x,y}\in X u_x v_y e(x)\boxdot e(y) := \sum_{x,y\in X}u_xv_ye(x\cdot y).$$

\begin{proposition}\label{prop:assocff}
    $(X,\cdot)$ é associativa se, e somente se, 
    $(\FF^X, \boxdot)$ é associativa. 
\end{proposition}
    \begin{proof}
        \begin{description}
            \item [($\Leftarrow$)] Basta notar que se $(X,\cdot)$ possui uma tripla 
            não associativa
            $(a,b,c)\in X^3$, então a tripla $(e(a),e(b),e(c))$ 
            é não associativa em $\FF^X$. De fato, 
            \begin{align*}
            (e(a)\boxdot e(b))\boxdot e(c) &= e(a\cdot b)\boxdot e(c)\\
             &= e((a\cdot b)\cdot c)\\&\not= e(a\cdot (b\cdot c)) \\
                & =e(a)\boxdot e(b\cdot c)\\  &= e(a)\boxdot (e(b)\boxdot e(c)).
            \end{align*}
            \item [($\Rightarrow$)] Suponha que $(X,\cdot)$ seja associativa. Então para quaisquer $u,v,w\in \FF^X$, temos 
            $$(u\boxdot v)\boxdot w = \sum_{x,y,z\in X}u_xv_yw_z e((x\cdot y)\cdot z)=
            \sum_{x,y,z\in X} e(x\cdot (y\cdot z)) = u\boxdot (v\boxdot w).$$
        \end{description}
    \end{proof}

    Agora estamos em condições de descrever um algoritmo que para verificar se 
    $(X,\cdot)$ é associativa. 
    \begin{algorithm}
    \caption{Algoritmo probabilístico para verificar se $(X,\cdot)$ é associativa}
    \begin{algorithmic}[1]
        \State Para todo $x\in X$, escolha $\alpha_x, \beta_x, \gamma_x\in \FF$ uniformemente ao acaso. 
            \State Faça $u\gets \sum_{x\in X} \alpha_xe(x)$, $v\gets \sum_{x\in X}\beta_xe(x)$, $w\gets\sum_{x\in X}\gamma_xe(x)$.
            \State \textbf{Se} {$u\boxdot(v\boxdot w) \not= (u\boxdot v)\boxdot w$}
            \State \hspace{6mm} \makebox[1.3cm]{\textbf{então }}\textbf{devolva } \textsc{Não-Associativa}.
            \State \hspace{6mm} \makebox[1.3cm]{\textbf{senão }}\textbf{devolva } \textsc{Associativa}.
    \end{algorithmic}
    \label{alg:assoc}
    \end{algorithm}

    Segue da Proposição~\ref{prop:assocff} que se $(X,\cdot)$ é associativa, então o algoritmo 
acima sempre dá a resposta correta. O resultado abaixo mostra que se $(X,\cdot)$ não 
é associativa, então o algoritmo dá a resposta correta com alta probabilidade. 

\begin{theorem}
    Suponha que $(X,\cdot)$ é não-associativa e sejam $u,v,z$ escolhidos como na linha~$2$ 
    do Algoritmo~\ref{alg:assoc}.  Então $\PP((u,v,w)\text{ ser associativa})\leq \frac12$.
\end{theorem}
\begin{proof}
    Seja $(a,b,c)\in X^3$ uma tripla não-associativa. Podemos supor, sem perda
    de generalidade, que $\alpha_a,\beta_b,\gamma_c$ são os últimos elementos a
    serem escolhidos no primeiro passo do algoritmo. Seja $r = (a\cdot (b\cdot
    c))$. Vamos mostrar que $$\PP((u\boxdot (v\boxdot w))_r \not= ((u\boxdot
    v)\boxdot w)_r)\leq \frac12,$$ da onde seguirá o resultado desejado. 

    Defina $f(\alpha_a,\beta_b,\gamma_c)=(u\boxdot (v\boxdot w))_r\in \FF[\alpha_a,\beta_b,\gamma_c]$ e 
    $g(\alpha_a,\beta_b,\gamma_c)=((u\boxdot v)\boxdot w)_r\in \FF[\alpha_a,\beta_b,\gamma_c]$. 
    Observe que $$f(\alpha_a,\beta_b,\gamma_c)=\sum_{\substack{x,y,z\in X\\x\cdot (y\cdot z)=r}}\alpha_x\beta_y\gamma_z$$
    e que $\alpha_a\beta_b\gamma_c$ aparece no polinômio $f$ com 
    coeficiente $1$. Logo $\partial{f}=3$. Além disso, como $(a,b,c)$ é não-associativa, segue 
    que $\alpha_a\beta_b\gamma_c$ não aparece no polinômio $g$ (caso contrário, teríamos 
    $(a\cdot b)\cdot c = r$). 

    Portanto, $f(\alpha_a,\beta_b, \gamma_c)-g(\alpha_a,\beta_b,\gamma_c)$ é um polinômio 
    não-nulo de~$3$ variáveis e grau~$3$. Aplicando o Lema~\ref{lemma:szprob}, obtemos 
    $$\PP(f(\alpha_a,\beta_b,\gamma_c)-g(\alpha_a,\beta_b,\gamma_c)=0)\leq \frac{3}{|F|}\leq \frac12,$$
    como desejado. 
\end{proof}








