\def\vec#1{\boldsymbol{#1}}
\subsection{Conjunto de Kakeya para Corpos Finitos}

Seja $\FF$ um corpo finito de tamanho $q=|\FF|$. Considere $\FF^n$ como um 
espaço vetorial. 

Dizemos que $K\subseteq \FF^n$ é um \emph{conjunto de Kakeya} se para todo
$\vec{u}\in \FF^n$, existe algum $\vec{a}\in\FF^n$ tal que $\vec{a}+t\vec{u}\in
K$ para todo $t\in\FF$.

\begin{theorem}[Duir 2009]\label{th:duir2009}
    Se $K$ é um conjunto de Kakeya, então $|K|\geq \binom{q+n-1}{n}$.
\end{theorem}

\begin{remark} Para $n$ fixo, a cota inferior do Teorema~\ref{th:duir2009}
corresponde a uma fração positiva do $\FF^n$. De fato, 
\[\binom{q+n-1}{n}\leq \(\binom{q+n-1}{n}\)^n \geq \(\frac{q}n\)^n =
\frac1{n^n} |\FF^n|.\]
\end{remark}

\begin{lemma}[Schartz-Zippel]\label{lemma:schartz-zippel}
    Seja $\FF$ um corpo finito, $S\subseteq \FF$ e 
    $p\in \FF[x_1,\dots,x_m]$ um polinômio de grau $\partial p\leq d$. 
    Seja \[R=\{\vec{r}=(r_1,\dots,r_m)\in S^m\: p(\vec{r})=0\}\]
    Então $|R|\leq d|S|^{m-1}$.
\end{lemma}
\begin{proof}
    Escreva $p$ como um polinômio sobre $x_1$, cujos coeficientes são 
    elementos não nulos $p_1,\dots,p_k\in \FF[x_2,\dots,x_m]$, i.e.
    \[p = \sum_{i=0}^k p_i(x_2,\dots,x_m) x_1^i.\]
    Seja $R_1 = \{\vec{r}=(r_1,\dots,r_m)\in R\: p_k(r_2,\dots,r_m)=0\}$. Por indução em $m$
    temos 
    \[\#\{(r_2,\dots,r_m)\in S^{m-1}\: p_k(r_2,\dots,r_m)=0\}\leq 
    (\partial p_k)|S|^{m-2}\leq (d-k)|S|^{m-2}.\]
    Portanto $|R_1|\leq |S|(d-k)|S|^{m-2} = (d-k)|S|^{m-1}$. 
    Seja $R_2=R\setminus R_1$.
    Se $\vec{r}=(r1,\dots,r_m)\in R_2$, então $p_k(r_2,\dots,r_m)\not=0$. 
    Portanto $\tilde{p}(x_1):=p(x_1,r_2,\dots,r_m)\in\FF[x_1]$ é 
    não-nulo e tem grau $k$. Assim, $\#\{r_1:\tilde{p}(r_1)=0\}\leq k$.
    Portanto $|R_2|\leq k|S|^{m-1}$. Logo, $|R|\leq d|S|^{m-1}$. 
\end{proof}
\begin{remark}
    Note que a cota dada pelo Lema de Schartz-Zippel vale com igualdade 
    se consideramos um polinômio $p'\in \FF[x_1]$ com $d$ raízes distintas
    e o estendemos a um polinômio $p\in \FF[x_1,\dots,x_m]$ (de modo 
    que $x_2,\dots,x_m$ são variáveis livres em $p$). 
\end{remark}

Seja $p(x_1,\dots,x_n)\in\FF[x_1,\dots,x_m]$ e denote $(x_1,\dots,x_n)$ por
$\vec{x}$. Escrevemos $\vec{x} = \sum c_{\vec{\alpha}} \vec{x}^{\vec{\alpha}}$,
onde a soma é sobre todas as $n$-uplas de inteiros 
$\vec{\alpha}=(\alpha_1,\dots,\alpha_n)$ satisfazendo $\sum_{i=1}^n\alpha_i\leq
\partial p$, $\vec{x}^{\vec{\alpha}}=x_1^{\alpha_1}\cdots x_n^{\alpha_n}$
e $c_{\alpha}\in\FF$. 

\begin{lemma}\label{lemma:kakeya1}
    Sejam $\vec{a}_1,\dots,\vec{a}_N\in\FF^n$ e $N<\binom{d+n}{n}$. Então existe
    polinômio não-nulo $p(x_1,\dots,x_n)\in\FF[x_1,\dots,x_n]$ com 
    $\partial p\leq d$ tal que $p(\vec{a}_i)=0$ para todo $1\leq i\leq N$. 
\end{lemma}
\begin{proof}
    Note que existem exatamente $\binom{d+n}{n}$ $n$-uplas de inteiros 
    $\vec{\alpha} = (a_i)_{i=1}^n$, $a_i\geq 0$, tais que 
    $\sum_{i=1}^n \alpha_i\leq d$. %TODO

    Queremos $p=\sum c_{\vec{\alpha}}x^{\alpha}$ com $\partial p\leq d$, 
    $p\not\equiv 0$ e $p(\vec{a}_i)=0$ para todo $1\leq i\leq N$. Tais
    restrições dão origem a um sistema linear homogêneo com 
    $\binom{d+n}{n}$ variáveis mas apenas $N<\binom{d+n}{n}$ 
    equações. Logo, existe $(c_{\vec{\alpha}})\not=0$ que é uma
    solução para o sistema. 
\end{proof}

\begin{proof}[Demonstração do Teorema~\ref{th:duir2009}]
    Suponha, por contradição, que existe $K\subseteq\FF^{n-1}$
    de Kakeya e $|K|<\binom{q+n-1}{n}$. Pelo Lema~\ref{lemma:kakeya1}, existe 
    polinômio $p(x_1,\dots,x_n)$ não-nulo com $\partial p\leq q-1$ e
    $p(\vec{a})=0$, para todo $\vec{a}\in K$. Fixe $\vec{u}\in
    \FF^n\setminus\{0\}$. Como $K$ é de Kakeya, existe $\vec{a}\in\FF^n$ tal 
    que $\vec{a}+t\vec{u}\in K$ para todo $t\in \FF$. Considere o polinômio
    $f(t)=p(\vec{a}+t\vec{u})$. Temos $\partial f\leq \partial p=d\leq q-1$.
    Ademais, $f(t)=0$ para todo $t\in \FF$. Segue que $f$ é o polinômio nulo. Em
    particular, o coeficiente $[t^d]f(t)$ de $t^d$ de $f$ é nulo. 
    Temos 
    \begin{align*}
        [t^d]f(t) &= [t^d]p(\vec{a}+t\vec{u}) 
        = [t^d]\sum_{\vec{a}:\sum \alpha_i\leq d}
        (\vec{a}+t\vec{u})^{\vec{\alpha}}\\
        &= [t^d]\sum_{\vec{a}:\sum \alpha_i= d} (\vec{a}+t\vec{u})^{\vec{\alpha}}
        = [t^d]\sum_{\vec{a}:\sum \alpha_i= d} (\vec{u})^{\vec{\alpha}}
        = \text{``parte homogênea de $p(\vec{x})$''}.
    \end{align*}
    Seja
    $\overline{p}(\vec{x})=\sum_{\vec{a}:\sum_{a_i}=d}c_{\vec{a}}\vec{x}^{\vec{\alpha}}$
    a parte homogênea de $p(\vec{x})$. Concluímos que $\overline{p}(\vec{u})=0$
    para todo $u\in\FF^n$. Mas pelo Lema de Schartz-Zippel temos
    $$\{\vec{r}\in\FF^n\:\overline{p}(r)=0\}\leq dq^{n-1}
    \leq (q-1)q^{n-1}<q^n = |\FF|^n,$$
    uma contradição.
\end{proof}
